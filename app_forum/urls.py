from django.conf.urls import url

from .views import index, get_forum_posts, post_forum

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^get-forum-posts/$', get_forum_posts, name="get-forum-posts"),
    url(r'^post-forum/$', post_forum, name="post-forum"),
]
