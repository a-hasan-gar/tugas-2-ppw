from django.test import TestCase
from django.urls import reverse, resolve
import json

from .views import index
from app_profile.models import Perusahaan
from app_forum.models import Forum

mock_perusahaan = Perusahaan(nama_perusahaan="Traveloka", tipe_perusahaan="IT", \
	web_perusahaan="traveloka.com", spesialitas_perusahaan="jalan-jalan!")


# Create your tests here.
class AppForumUnitTest(TestCase):
	def test_index_log_in(self):
		found = resolve("/forum/")
		self.assertEqual(found.func, index)

		mock_perusahaan.save()
		response = self.client.get("/forum/")
		self.assertEqual(response.status_code, 200)

	def test_get_forum_posts_without_session(self):
		mock_perusahaan.save()
		response = self.client.get("/forum/get-forum-posts")
		self.assertRedirects(response, reverse('forum:get-forum-posts'), 301, 302)

	def test_get_forum_posts_negative_page(self):
		page = -1
		mock_perusahaan.save()
		session = self.client.session
		session["perusahaan_id"] = 1
		session.save()

		response = self.client.get("/forum/get-forum-posts/?page=" + str(page))
		json_object = json.loads(response.content.decode("utf-8"))
		self.assertEqual(not json_object["forums"], True)

	def test_get_forum_posts(self):
		mock_perusahaan.save()
		string = "Dicari Web Developer yang menyukai PPW"
		forum = Forum(perusahaan=mock_perusahaan, content=string)
		forum.save()
		session = self.client.session
		session["perusahaan_id"] = 1
		session.save()

		response = self.client.get("/forum/get-forum-posts/")
		json_object = json.loads(response.content.decode("utf-8"))
		self.assertEqual(len(json_object["forums"]), 1)
		self.assertEqual(json_object["forums"][0]["content"], string)

	def test_post_forum(self):
		mock_perusahaan.save()
		session = self.client.session
		session["perusahaan_id"] = 1
		session.save()

		string = "saya suka web developing, tolong hire saya!"
		data = {"content" : string}
		response = self.client.post("/forum/post-forum/", data)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(Forum.objects.count(), 1)
		self.assertEqual(Forum.objects.get().content, string)
