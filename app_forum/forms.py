from django import forms

class Forum_Form(forms.Form):
	error_messages = {
		'required': 'Tolong isi input ini',
	}
	content_attrs = {
		'type': 'text',
		'cols': 100,
		'rows': 10,
		'class': 'todo-form-textarea',
		'placeholder':'Masukkan isi dari forum...'
	}

	content = forms.CharField(label='', required=True, max_length=1000, widget=forms.Textarea(attrs=content_attrs))
	