"""tugas_2_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

import app_login.urls as login
import app_profile.urls as app_profile
import app_forum.urls as app_forum

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^app-login/', include(login, namespace = 'app-login')),
    url(r'^forum/', include(app_forum, namespace="forum")),
    url(r'^profile/', include(app_profile,namespace='profile')),
    url(r'^$', RedirectView.as_view(url = '/app-login/', permanent='true'), name='index')
]
