from django.db import models

class Perusahaan(models.Model):
    nama_perusahaan = models.CharField(max_length=200)
    tipe_perusahaan = models.CharField(max_length=200)
    web_perusahaan = models.CharField(max_length=200)
    spesialitas_perusahaan = models.CharField(max_length=200)
    pic_perusahaan = models.CharField(default="", max_length=500)

