from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
# Create your tests here.
class AppLoginUnitTest(TestCase):
	def test_app_login_url_is_exist(self):
		response = Client().get('/app-login/')
		self.assertEqual(response.status_code, 200)

	def test_app_login_using_index_func(self):
		found = resolve('/app-login/')
		self.assertEqual(found.func, index)
