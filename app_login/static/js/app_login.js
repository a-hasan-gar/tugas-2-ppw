// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}
  // Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
  }
// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    var user = data.values[0];
    //$("#name").append('Logged in as '+user.firstName+' '+user.lastName);
    //document.getElementById('profileData').style.display = 'block'; 
    $("#button").html('<button class="btn" onclick="logout()">LOGOUT</button>');
}

// Use the API call wrapper to request the company's profile data
function getCompanyData() {     
	// Masukin ID company lau
    var cpnyID = 13601891;

    IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description)?format=json")
      .method("GET")
      .result(dipslayCompanyData)
      .error(onError);
}

function displayCompanyData(data){
    // To do
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    $("#button").html('');
    IN.User.logout(removeProfileData);

}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}